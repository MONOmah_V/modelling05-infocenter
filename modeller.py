__author__ = 'monomah'


import numpy.random as nr


class UniformGenerator:
    def __init__(self, m, d):
        self._a = m - d
        self._b = m + d
        if not 0 <= self._a <= self._b:
            raise ValueError('Параметры должны удовлетворять условию 0 <= a <= b')

    def next(self):
        return nr.uniform(self._a, self._b)


class ConstGenerator:
    def __init__(self, m):
        if m <= 0:
            raise ValueError('Параметр должен быть больше 0')
        self._m = m

    def next(self):
        return self._m


class RequestGenerator:
    def __init__(self, generator):
        self._generator = generator
        self._receivers = set()
        self._generated_requests = 0
        self._last_time_period = 0

    @property
    def last_time_period(self):
        return self._last_time_period

    @property
    def generated_requests(self):
        return self._generated_requests

    def add_receiver(self, receiver):
        self._receivers.add(receiver)

    def remove_receiver(self, receiver):
        try:
            self._receivers.remove(receiver)
        except KeyError:
            pass

    def next_time_period(self):
        self._last_time_period = self._generator.next()
        return self._last_time_period

    def emit_request(self):
        self._generated_requests += 1
        for receiver in self._receivers:
            if receiver.receive_request():
                return receiver
        else:
            return None


class RequestProcessor(RequestGenerator):
    def __init__(self, generator, *, has_queue=True, reenter_probability=0):
        super().__init__(generator)
        self._generator = generator
        self._has_queue = has_queue
        self._current_queue_size = 0
        self._max_queue_size = 1
        self._processed_requests = 0
        self._reenter_probability = reenter_probability
        self._reentered_requests = 0

    @property
    def processed_requests(self):
        return self._processed_requests

    @property
    def max_queue_size(self):
        return self._max_queue_size

    @property
    def current_queue_size(self):
        return self._current_queue_size

    @property
    def reentered_requests(self):
        return self._reentered_requests

    def process(self):
        if self._current_queue_size > 0:
            self._processed_requests += 1
            self._current_queue_size -= 1
            self.emit_request()
            if nr.random_sample() < self._reenter_probability:
                self._reentered_requests += 1
                self.receive_request()

    def receive_request(self):
        if not self._has_queue and self._current_queue_size >= self._max_queue_size:
            return False
        elif self._has_queue and self._current_queue_size >= self._max_queue_size:
            self._max_queue_size += 1
        self._current_queue_size += 1
        return True

def event_based_modelling(client_m, client_d,
                          op0_m, op0_d, op1_m, op1_d, op2_m, op2_d,
                          comp0_m, comp1_m, c_count):
    client_gen = RequestGenerator(UniformGenerator(client_m, client_d))
    op0 = RequestProcessor(UniformGenerator(op0_m, op0_d), has_queue=False)
    op1 = RequestProcessor(UniformGenerator(op1_m, op1_d), has_queue=False)
    op2 = RequestProcessor(UniformGenerator(op2_m, op2_d), has_queue=False)
    comp0 = RequestProcessor(ConstGenerator(comp0_m))
    comp1 = RequestProcessor(ConstGenerator(comp1_m))

    client_gen.add_receiver(op0)
    client_gen.add_receiver(op1)
    client_gen.add_receiver(op2)
    op0.add_receiver(comp0)
    op1.add_receiver(comp0)
    op2.add_receiver(comp1)

    instances = [client_gen, op0, op1, op2, comp0, comp1]
    for instance in instances:
        instance._time = 0

    dropped_requests = 0
    client_gen._time = client_gen.next_time_period()
    op0._time = op0.next_time_period()
    while client_gen.generated_requests < c_count:
        current_time = client_gen._time
        for instance in instances:
            if 0 < instance._time < current_time:
                current_time = instance._time

        if current_time == client_gen._time:
            assigned_processor = client_gen.emit_request()
            if assigned_processor is not None:
                assigned_processor._time = current_time + assigned_processor.next_time_period()
            else:
                dropped_requests += 1
            client_gen._time = current_time + client_gen.next_time_period()
        if current_time == op0._time:
            op0.process()
            if comp0.current_queue_size == 0:
                comp0._time = current_time + comp0.next_time_period()
            op0._time = 0
        if current_time == op1._time:
            op1.process()
            if comp0.current_queue_size == 0:
                comp0._time = current_time + comp0.next_time_period()
            op1._time = 0
        if current_time == op2._time:
            op2.process()
            if comp1.current_queue_size == 0:
                comp1._time = current_time + comp1.next_time_period()
            op2._time = 0
        if current_time == comp0._time:
            comp0.process()
            if comp0.current_queue_size == 0:
                comp0._time = 0
            else:
                comp0._time = current_time + comp0.next_time_period()
        if current_time == comp1._time:
            comp1.process()
            if comp1.current_queue_size == 0:
                comp1._time = 0
            else:
                comp1._time = current_time + comp1.next_time_period()
    return dropped_requests / c_count


def __event_based_modelling(self, request_count):
    generator = self._generator
    processor = self._processor

    gen_period = generator.next_time_period()
    proc_period = gen_period + processor.next_time_period()
    while processor.processed_requests < request_count:
        if gen_period <= proc_period:
            generator.emit_request()
            gen_period += generator.next_time_period()
        if gen_period >= proc_period:
            processor.process()
            if processor.current_queue_size > 0:
                proc_period += processor.next_time_period()
            else:
                proc_period = gen_period + processor.next_time_period()

    return (processor.processed_requests, processor.reentered_requests,
            processor.max_queue_size, proc_period)
